export default {
  getTodos: state => {
    return state.todos
  },
  getTodosCount: (state, getters) => {
    return getters.getTodos.length
  },
}
