import request from 'axios'

request.defaults.baseURL = `http://localhost:8080/api`

export default {
  async GET_TODOS ({commit}) {
    await request.get(`/todos`).then(async (response) => {
      if (response.status === 200) {
        let data = await response.data
        commit('SET_TODOS', {data})
      }
    }).catch((error) => {
      console.log(error)
    })
  },

  async CREATE_TODO ({commit}, todo) {
    await request.post(`/todos`, todo).then(async (response) => {
      if (response.status === 201) {
        let data = await response.data
        commit('CREATE_TODO', {data})
      }
    }).catch((error) => {
      console.log(error)
    })
  },

  async EDIT_TODO ({commit}, todo) {
    await request.patch(`/todos/${todo.id}`, todo).then(async (response) => {
      if (response.status === 200) {
        let data = await response.data
        commit('EDIT_TODO', {data})
      }
    }).catch((error) => {
      console.log(error)
    })
  },
  async DELETE_TODO ({commit}, todo) {
    await request.delete(`/todos/${todo.id}`).then(async (response) => {
      if (response.status === 202) {
        let data = await response.data
        commit('DELETE_TODO', {data})
      }
    }).catch((error) => {
      console.log(error)
    })
  },
}