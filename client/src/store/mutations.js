import findIndex from 'lodash/findIndex'

export default {
  SET_TODOS: (state, todos) => {
    state.todos = todos.data
  },
  CREATE_TODO: (state, todos) => {
    state.todos.push(todos.data)
  },
  EDIT_TODO: (state, todos) => {
    let index = findIndex(state.todos, ['id', todos.data.id])
    state.todos[index] = todos.data
  },
  DELETE_TODO: (state, todos) => {
    let index = findIndex(state.todos, ['id', todos.data.id])
    state.todos.splice(index, 1)
  },
}
