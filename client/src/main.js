/**
 * @author: Akos Horvath
 * @email : horvath.akos.7@gmail.com
 * @desc  : IdomSoft test
 */

import Vue from 'vue'
import App from './App.vue'
import store from './store'
import VueMaterial from 'vue-material'

Vue.use(VueMaterial)

new Vue({
  el: '#app',
  store,
  ...App,
})
