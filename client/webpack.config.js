const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')

module.exports = (env, options) => {
  const isProd = options.mode === 'production'

  return {
    entry: {
      app: [
        path.resolve(__dirname, 'src', 'assets', 'style.scss'),
        path.resolve(__dirname, 'src', 'main'),
      ],
    },
    output: {
      filename: 'js/[name].[hash].js',
      chunkFilename: 'js/[id].[hash].js',
      path: path.resolve(__dirname, 'dist'),
      publicPath: '/',
    },
    resolve: {
      extensions: ['*', '.vue', '.js'],
      modules: ['node_modules'],
      alias: {
        'assets': path.join(__dirname, 'src', 'assets'),
      },
    },
    devtool: '#source-maps',
    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: 'vue-loader',
        },
        {
          test: /\.js$/,
          loader: 'babel-loader',
          include: [path.join(__dirname, 'src'), path.join(__dirname, '../src')],
        },
        {
          test: /\.css$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: 'css-loader',
          }),
        },
        {
          test: /\.scss$/,
          use: [
            {
              loader: 'style-loader',
            },
            {
              loader: 'css-loader',
            },
            {
              loader: 'sass-loader',
            },
          ],
        },
        {
          test: /\.json$/,
          loader: 'json-loader',
        },
        {
          test: /\.(png|jpe?g|gif)(\?.*)?$/,
          loader: 'url-loader',
          query: {
            limit: 10000,
          },
        },
        {
          test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: 'url-loader',
          query: {
            limit: 10000,
            mimetype: 'application/font-woff',
          },
        },
        {
          test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: 'url-loader',
          query: {
            limit: 10000,
          },
        },
      ],
    },
    plugins: [
      new CleanWebpackPlugin('./dist'),
      new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'src', 'index.html'),
      }),
      new ExtractTextPlugin('[name].css'),
      isProd ? new OptimizeCssAssetsPlugin({
        cssProcessor: require('cssnano'),
        cssProcessorPluginOptions: {
          preset: ['default', {discardComments: {removeAll: true}}],
        },
        canPrint: true,
      }) : '',
      new VueLoaderPlugin(),
    ],
  }
}
