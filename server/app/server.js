const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const history = require('connect-history-api-fallback')
const _ = require('lodash')

let todos = []
let todoNextId = 1

module.exports = (host, port) => {
  const app = express()

  const router = new express.Router()
  app.use('/', router)
  app.use('/fonts', express.static(path.join(__dirname, '../../client/src/assets/fonts')))

  // Router config
  router.use(
    bodyParser.urlencoded({extended: true}), // Parse application/x-www-form-urlencoded
    bodyParser.json(), // Parse application/json
  )

  /**
   * Az osszes todo lekerdezese
   */
  router.get('/api/todos', function (req, res) {
    res.status(200).json(todos)
  })

  /**
   * Todo lekerdezese id alapjan
   */
  router.get('/api/todos/:todoId', function (req, res) {
    let matchedTodo = searchTodo(req.params.id, false)
    if (matchedTodo) {
      res.status(200).json(matchedTodo)
    }
    res.status(404).send()
  })

  /**
   * Todo letrehozasa
   */
  router.post('/api/todos/', function (req, res) {
    let todoBody = req.body

    if (!todoBody) {
      return res.status(400).send()
    }
    todoBody['id'] = todoNextId++
    todos.push(todoBody)

    res.status(201).json(todoBody)
  })

  /**
   * Todo modositasa
   */
  router.patch('/api/todos/:todoId', function (req, res) {
    let matchedTodo = searchTodo(req.params.todoId, true)
    if (matchedTodo < 0) {
      res.status(404).send()
    }
    todos[matchedTodo] = req.body
    res.status(200).json(todos[matchedTodo])
  })

  /**
   * Todo torlese
   */
  router.delete('/api/todos/:todoId', function (req, res) {
    let deletedTodo = deleteTodo(req.params.todoId, false)
    if (!deletedTodo) {
      res.status(404).send()
    }
    res.status(202).json(deletedTodo)
  })

  // History fallback api
  router.use(history())
  // Kliens kod bundle betoltese, ha van
  router.use('/', express.static(path.join(__dirname, '../../client/dist'))) // History fallback utan kell megadni

  /**
   * Search for todo
   * @param id - Todo id
   * @param searchForIndex - Search for index or item
   */
  function searchTodo (id, searchForIndex) {
    let todoId = parseInt(id)
    return searchForIndex ? _.findIndex(todos, ['id', todoId]) : _.find(todos, ['id', todoId])
  }

  /**
   * Delete todo from list
   * @param id - Todo id
   * @param searchForIndex - Search for index or item
   * @returns {*}
   */
  function deleteTodo (id, searchForIndex) {
    let matchedTodo = searchTodo(id, searchForIndex)
    if (matchedTodo) {
      _.remove(todos, (item) => {
        return item.id === matchedTodo.id
      })
      return matchedTodo
    }
    return false
  }

  return app.listen(port, host, () => {
    console.info(`IdomSoft test web server started on ${host}:${port}`)
  })
}
